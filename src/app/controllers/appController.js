class AppController {

    constructor() {
        'ngInject';
        this.nombreAplicacion = 'Coje tus seis!';
        this.nombreEmpresa = 'La capital deportiva';
        this.showSideBar = true;
        this.showResSideBar = false;
        this.showNotificaciones = false;

        var w = window.innerWidth;
        if(w > 960){
            this.desktopSize = true;
        } else {
            this.deviceSize = true;
        }

        window.onresize = (event) => {
            var w = window.innerWidth;
            if(w > 960){
                this.desktopSize = true;
            } else {
                this.deviceSize = true;
            }
        };
    }

    toggleSideBar() {
        this.showSideBar = !this.showSideBar;
    }

    toggleResSidebar() {
        this.showResSideBar = !this.showResSideBar;
        this.showNotificaciones = false;
    }

    toggelResNotifications() {
        this.showNotificaciones = !this.showNotificaciones;
        this.showResSideBar = false;
    }

    toggleAll(){
        this.showNotificaciones = false;
        this.showResSideBar = false;
    }

}

export default AppController;
