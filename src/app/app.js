import angular from 'angular';

// Estilos
import '../style/app.css';
import '../style/ionicons.css';
import '@uirouter/angularjs';
import 'angular-animate';
import 'angular-touch';
import 'angular-ui-bootstrap';
import 'angular-ui-bootstrap/dist/ui-bootstrap-csp.css';

// Subcomponentes
import  './components/prueba';
import  './components/login';
import  './components/admin';

// Directivas
import ValidationInput from './directives/validationInput';
import MenuButton from './directives/menuButton';
import CollapsableMenuButton from './directives/collapsableMenuButton';
import NotificationMenu from './directives/notificationMenu';
import NotificationMenuItem from './directives/notificationMenuItem';

// Controller
import AppController from './controllers/appController';
// Templetas
import bodyTpl from './views/app.html';

let app = angular.module('app', [
  'ui.router',
  'ui.bootstrap', 
  'ngAnimate', 
  'ngTouch',


  'app.login',
  'app.admin'
])
.controller('appController', AppController)
.directive('validationInput', () => new ValidationInput)
.directive('menuButton', () => new MenuButton)
.directive('collapsableMenuButton', () => new CollapsableMenuButton)
.directive('notificationMenu', () => new NotificationMenu)
.directive('notificationMenuItem', () => new NotificationMenuItem)
;

app.config(($stateProvider, $urlRouterProvider, $locationProvider) => {
  $locationProvider.html5Mode(false).hashPrefix('');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('app', {
      url: '/',
      // abstract: true,
      template: bodyTpl, 
      controller: 'appController',
      controllerAs: 'appCtl'
    })
    ;

})

export default app;