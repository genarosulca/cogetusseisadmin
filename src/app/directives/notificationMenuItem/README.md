## Notification Menu Item
Esta directiva es para utiliazar dentro de *NotificationMenu*, lo que hace es crear el boton para el mismo. Se utiliza de la siguiente manera: 
### HTML
```
<notification-menu-item 
    type="success" 
    from="Juan" 
    title="Saludos amigo" 
    date="Hace 5 horas"
    ng-click="ctrl.action()"
    readed="ctrl.readed">
</notification-menu-item>
```
Los atributos son los siguientes:


|Atributo        |Descripcion							|
|----------------|-------------------------------|
|type			|Tipo de notificacion, define el icono y el color, tipos disponiles: *error*, *success*			|
|from			|Nombre de la persona que genera la notificacion	|
|title           |Titulo de la notificacion generada |
|date           |Momento en el que se creo la notificacion. Enviar *string* a presentar    |
|ng-click		|Accion a realizarse al dar clic en la notificacion	|
|readed			|Bandera que indica si la notificacion ha sido leida o no   |
