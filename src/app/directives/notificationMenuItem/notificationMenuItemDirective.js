// Importo controlador
import NotificationMenuItemController from './controllers/notificationMenuItemController';
// Importo template
import notificationMenuItemTpl from './views/notificationMenuItemView.html';

import './assets/styles.css'

// Defino directiva
class notificationMenuItemDirective {
  constructor() {
    this.restrict = 'E';
    this.scope = {
      type: '@',
      from: '@',
      title: '@',
      date: '@',
      ngClick: '&ngClick',
      readed: '='
    };
    this.controller = NotificationMenuItemController;
    this.controllerAs = "ctrl";
    this.bindToController = true;
    this.template = notificationMenuItemTpl;
  }
}
export default notificationMenuItemDirective;