## Menu Button
Esta directiva es para crear un boto en el menu. Se utiliza de la siguiente manera: 
### HTML
```
<menu-button 
    label="Dashboard" 
    ng-click="ctrl.action()" 
    icon="ion-icon" 
    active="false" 
    collapsable="false" 
    showSubmenu="ctrl.showSubmenu">
</menu-button>
```
Los atributos son los siguientes:


|Atributo        |Descripcion							|
|----------------|-------------------------------|
|label			|Label del boton		|
|ng-click		|Accion a realizar en clic	|
|icon           |Icono   |
|active         |*true* o *false* a actualizar cuando se necesite que este como activo el boton    |
|collapsable	|*true* o *false* si el boton va a ser un menu collapsable	|
|showSubmenu	|*true* o *false* si el boton va a ser un menu collapsable   |
