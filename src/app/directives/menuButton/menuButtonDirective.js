// Importo controlador
import MenuButtonController from './controllers/menuButtonController';
// Importo template
import menuButtonTpl from './views/menuButtonView.html';

import './assets/styles.css'

// Defino directiva
class menuButtonDirective {
  constructor() {
    this.restrict = 'E';
    this.scope = {
      label: '@',
      ngClick: '&ngClick',
      icon: '@',
      active: '=',
      collapsable: '=',
      showSubmenu: '='
    };
    this.controller = MenuButtonController;
    this.controllerAs = "ctrl";
    this.bindToController = true;
    this.template = menuButtonTpl;
  }
}
export default menuButtonDirective;