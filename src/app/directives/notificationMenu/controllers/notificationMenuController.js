class NotificationMenuController {

    constructor($timeout) {
        'ngInject';
        $timeout(() => {
            let todayDate = new Date();

            this.today = {
                horas: todayDate.getHours(),
                minutos: todayDate.getMinutes(),
                dia: todayDate.getDate(),
                mes: todayDate.getMonth(),
                anio: todayDate.getYear(),
                semana: this.getWeekNumber(todayDate)
            }
            this.notificaciones.forEach(element => {
                var calcAnios = this.today.anio - element.date.getYear();
                var calcMeses = this.today.mes - element.date.getMonth();
                var calcDias = this.today.dia - element.date.getDate();
                var calcHoras = this.today.horas - element.date.getHours();
                var calcMinutos = this.today.minutos - element.date.getMinutes();
                element.semana = this.getWeekNumber(element.date);
                var calcSemanas = this.today.semana - element.semana ;
                if (calcAnios > 0) {
                    if(calcAnios == 1) {
                        if(calcMeses == 0){
                            element.fecha = 'Hace ' + calcAnios + ' año' + this.getPlural(calcAnios, false);
                        } else if(calcMeses < 0){
                            var calcMesAbs = 12 - Math.abs(calcMeses);
                            element.fecha = 'Hace ' + calcMesAbs + ' mes' + this.getPlural(calcMesAbs, true);
                        }
                    } else if (calcAnios > 1){
                        element.fecha = 'Hace ' + calcAnios + ' año' + this.getPlural(calcAnios, false);
                    }
                } else if (calcMeses > 0) {
                    element.fecha = 'Hace ' + calcMeses + ' mes' + this.getPlural(calcMeses, true);
                }else if (calcSemanas > 0){
                    
                    if(calcSemanas > 1 ){
                        element.fecha = 'Hace ' + calcSemanas + ' semana' + this.getPlural(calcSemanas, false);
                    }else {
                        if (calcDias > 7) {
                            element.fecha = 'Hace ' + calcSemanas + ' semana' + this.getPlural(calcSemanas, false);
                        } else {
                            if (calcDias == 1) {
                                element.fecha = 'Ayer';
                            } else {
                                element.fecha = 'Hace ' + calcDias + ' dia' + this.getPlural(calcDias, false);
                            }
                        }
                    }
                } else if (calcDias > 0) {
                    if (calcDias == 1) {
                        element.fecha = 'Ayer';
                    } else {
                        element.fecha = 'Hace ' + calcDias + ' dia' + this.getPlural(calcDias, false);
                    }
                } else if (calcHoras > 0) {
                    element.fecha = 'Hace ' + calcHoras + ' hora' + this.getPlural(calcHoras, false);
                } else if (calcMinutos > 0) {
                    element.fecha = 'Hace ' + calcMinutos + ' minuto' + this.getPlural(calcMinutos, false);
                } else if (calcMinutos == 0) {
                    element.fecha = 'Hace un momento';
                }
            });
        }, 100)
    }

    getPlural(numero, mes) {
        if (numero > 1) {
            if (mes) {
                return 'es';
            } else {
                return 's';
            }
        } else {
            return '';
        }
    }

    getWeekNumber(d){
        // var d = new Date(Date.UTC(2018, 0, 16));
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
      };
}

export default NotificationMenuController;
