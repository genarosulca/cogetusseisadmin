// Importo controlador
import NotificationMenuController from './controllers/notificationMenuController';
// Importo template
import notificationMenuTpl from './views/notificationMenuView.html';

import './assets/styles.css'

// Defino directiva
class notificationMenuDirective {
  constructor() {
    this.restrict = 'E';
    this.scope = {
      notificaciones: '=',
      verTodas: '&'
    };
    this.controller = NotificationMenuController;
    this.controllerAs = "ctrl";
    this.bindToController = true;
    this.template = notificationMenuTpl;
  }
}
export default notificationMenuDirective;