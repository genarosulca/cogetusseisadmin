
## Notification Menu
Esta directiva es para crear el menu de notificaciones. Se utiliza de la siguiente manera: 
### HTML
```
<notification-menu 
	notificaciones="ctrl.notificaciones">
</notification-menu>
```
Los atributos son los siguientes:


|Atributo        |Descripcion							|
|----------------|-------------------------------|
|notificaciones	 |Coleccion con notificaciones	|
|verTodas       |Funcion a ejecutarse en click en ver todas     |

La **coleccion con notificaciones** se debe enviar de la siguiente manera:
```
[
   {
       type: 'error',
       from: 'Juan Sulca',
       title: 'Para que quiere saber eso. Saludos.',
       date: new Date(),
       action: () => {
           console.log('Ver notificacion error')
       },
       readed: false
   },
   {
       type: 'success',
       from: 'Fernando',
       title: 'Que te vaya muy bien mijin',
       date: new Date(this.minutos),
       action: () => {
           console.log('Ver notificacion Fernando')
       },
       readed: false
   }, 
]
```
Para los items de las notificaciones referirse a la documentacion de ***NotificationMenuItem***