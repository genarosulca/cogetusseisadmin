## Menu Button
Esta directiva es para crear un boton collapsable en el menu. Se utiliza de la siguiente manera: 
### HTML
```
<collapsable-menu-button 
    label="Complejo" 
    icon="ion-ios-football"
    options="ctrl.menuComplejo">
</collapsable-menu-button>
```
Los atributos son los siguientes:


|Atributo        |Descripcion							|
|----------------|-------------------------------|
|label			|Label del boton		|
|icon           |Icono   |
|options         |Se envia las opciones del menu cuando se abra    |

La lista de opciones **options** se debe enviar de la siguiente manera
```
[
    {
        label: 'Franjas horarias',
        action: () => {
            this.navegar('franjasHorarias')
        },
        active: this.menuItems.franjasHorarias.active
    }, {
        label: 'Administrar canchas',
        action: () => {
            this.navegar('administrarCanchas')
        },
        active: this.menuItems.administrarCanchas.active
    }
]
 ```
 **Nota**
Para *active* se debe asociar a un dato que pueda cambiarse para cambiar entre activo e inactivo los botones