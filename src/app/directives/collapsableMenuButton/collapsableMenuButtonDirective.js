// Importo controlador
import CollapsableMenuButtonController from './controllers/collapsableMenuButtonController';
// Importo template
import collapsableMenuButtonTpl from './views/collapsableMenuButtonView.html';

import './assets/styles.css'

// Defino directiva
class collapsableMenuButtonDirective {
  constructor() {
    this.restrict = 'E';
    this.scope = {
      label: '@',
      icon: '@',
      options: '='
    };
    this.controller = CollapsableMenuButtonController;
    this.controllerAs = "ctrl";
    this.bindToController = true;
    this.template = collapsableMenuButtonTpl;
  }
}
export default collapsableMenuButtonDirective;