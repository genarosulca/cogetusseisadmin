class CollapsableMenuButtonController {

    constructor($timeout) {
        'ngInject';
        this.showSubmenu = false;
        $timeout(() => {
            // Para que al recargar se abra automaticamente el collapsabe
            this.options.forEach(element => {
                if(element.active){
                    this.showSubmenu = true;
                }
            });
        }, 1)
        
    }

    toggle(){
        this.showSubmenu = !this.showSubmenu;
    }
}

export default CollapsableMenuButtonController;
