class ValidationInputController {

    constructor($timeout, $document) {
        'ngInject';
        this.formField = { $error: {} };
        $timeout(() => {
            if (this.type != 'radio' && this.type != 'checkbox') {
                this.formField = this.form[this.field];
                if (!angular.isDefined(this.requiredMsg)) {
                    this.requiredMsg = this.label + ' es requerido';
                }
                if (!angular.isDefined(this.type)) {
                    this.type = 'text';
                }
            }
            if (this.type == 'radio') {
                this.ngModel = this.options[0].value;
            }
            
            if(this.type == 'checkbox' && angular.isDefined(this.form)){
                console.log(this.field);
                this.formField = this.form[this.field];
                console.log(this.formField);
            }
        }, 0)

    }

    getInvalid() {
        if(angular.isDefined(this.form)){
            return this.formField.$invalid && (this.form.$submitted || this.formField.$dirty)
        }
    }

    showError(tipoErr) {
        if(angular.isDefined(this.form)){
            return this.formField.$error[tipoErr] && (this.form.$submitted || this.formField.$dirty)
        }
    }
}

export default ValidationInputController;
