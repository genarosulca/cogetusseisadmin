## Validation input
Esta directiva se utiliza en un form para poner el input con su respectiva label, sirve para text(email, password, etc), radio y checkbox. Se utiliza de la siguiente manera: 
### HTML
```
<validation-input 
    label="Usuario" 
    field="username"
    type="text"
    disabled="false"
    value="ctrl.user.username"
    form="loginForm"
    error-list="ctrl.errores" 
    required-msg="El campo usuario es requerido">
</validation-input>
```
Los atributos son los siguientes:


|Atributo        |Descripcion							|
|----------------|-------------------------------|
|label			|Label para el input			|
|field			|Nombre para el id del input(se crea como propiedad del form)	|
|type           |Opcional. El tipo para el input (HTML input types). Ejemplo: *password*, *maxlength*, *email*, etc     |
|disabled       |Opcional. *True* o *False* para desactivar o no el input    |
|maxlength		|Opcional. Validacion HTML de tamaño maximo	|
|value			|Modelo a bindear|
|form			|Nombre del form para realizar validaciones	|
|error-list		|Opcional. Lista de errores personalizados*			|
|required-msg	|Opcional. Mensaje de error para required*.	|
|options		|Para *radio* y *checkbox*

* La **lista de errores personalizados** se debe enviar de la siguiente manera
```
[
	{
		tipo: 'email'; // Para validaciones HTML
		msg: 'Texto ingresado no es un email valido'
	}
	{
		tipo: 'customerror',
		msg: 'Este es el mensaje para un error personalizado'
	}
]
 ```
Y al momento de hacer la validacion en angular:
```
validarForm(form) {
	form.field.$setValidity("customerror", false);
}
```
* De no ser enviado un mensaje para **required** se pondra por defecto *label* + *es requerido*.  Por ejemplo:
```
label: Nombre
Mensaje por defecto: Nombre es requerido
```
Para el tipo *radio* y *checkbox* se debe enviar en **options**:
```
{
	label: 'Valor 1',
	value: 'val1'
},{
	label: 'Valor 2',
	value: 'val2'
}
```
Las validaciones no estan disponibles para radio, para checkbox, si se desea validacion se debe enviar el parametro **form** y se agrega el error de la misma forma que para error personalizado