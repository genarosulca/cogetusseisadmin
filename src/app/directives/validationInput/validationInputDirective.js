// Importo controlador
import ValidationInputController from './controllers/validationInputController';
// Importo template
import validationInputTpl from './views/validationInputView.html';

import './assets/styles.css'

// Defino directiva
class validationInputDirective {
  constructor() {
    this.restrict = 'E';
    this.scope = {
      label: '@',
      field: '@',
      type: '@',
      ngModel: '=',
      disabled: '=',
      maxlength: '@',
      options: '=',

      form: '=',
      errorList: '=',
      requiredMsg: '@'
    };
    this.controller = ValidationInputController;
    this.controllerAs = "ctrl";
    this.bindToController = true;
    this.template = validationInputTpl;
  }
}
export default validationInputDirective;