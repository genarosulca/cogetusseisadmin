import pruebaModule from './pruebaModule';

import PruebaCenterController from './controllers/pruebaCenterController';
pruebaModule.controller('pruebaCenterController', PruebaCenterController);
import PruebaHeaderController from './controllers/pruebaHeaderController';
pruebaModule.controller('pruebaHeaderController', PruebaHeaderController);
import PruebaSideBarController from './controllers/pruebaSideBarController';
pruebaModule.controller('pruebaSideBarController', PruebaSideBarController);

export default pruebaModule;
