import angular from 'angular';

import '@uirouter/angularjs';


import pruebaCenterTpl from './views/pruebaViewCenter.html';
import pruebaHeaderTpl from './views/pruebaViewHeader.html';
import pruebaSideBarTpl from './views/pruebaViewSideBar.html';

let pruebaModule = angular.module('app.prueba', [
    'ui.router',
]);


pruebaModule.config(($stateProvider, $urlRouterProvider) => {

    $stateProvider
        .state('app.prueba', {
            url: 'asd',

            views: {
                'header@app': {
                    controller: 'pruebaHeaderController',
                    controllerAs: 'pruebaHeader',
                    // template: '<h1>HEADER</h1>'
                    templateUrl: pruebaHeaderTpl
                },
                'sidebar@app': {
                    controller: 'pruebaSideBarController',
                    controllerAs: 'pruebaSideBar',
                    template: pruebaSideBarTpl
                },
                'center@app': {
                    controller: 'pruebaCenterController',
                    controllerAs: 'pruebaCenter',
                    template: pruebaCenterTpl
                }
            }

            // controller: 'pruebaController',
            // controllerAs: 'prueba',
            // template: pruebaTpl
        })
        ;

});

export default pruebaModule;