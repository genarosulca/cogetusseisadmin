class NotificacionesModel {
    constructor() {
        this._notificaciones = {};
        this._nuevas = 0;
    }

    get notificaciones(){
        return this._notificaciones;
    }

    set notificaciones(notificaiciones){
        this._notificaciones = notificaiciones;
    }

    get nuevas(){
        return this._nuevas;
    }

    set nuevas(nuevas){
        this._nuevas = nuevas;
    }
 }
 
 export default NotificacionesModel;