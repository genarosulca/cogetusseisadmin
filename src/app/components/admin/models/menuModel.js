class MenuModel {
    constructor() {
        this._menu = {};
        this._complejoDeportivo = {};
    }

    get menu(){
        return this._menu;
    }

    set menu(menu) {
        this._menu = menu;
    }

    get complejoDeportivo(){
        return this._complejoDeportivo
    }

    set complejoDeportivo(complejoDeportivo){
        this._complejoDeportivo = complejoDeportivo;
    }

    setActiveItem(item) {
        for (var key in this._menu) {
            if (key == item) {
                this._menu[key].active = true;
            } else {
                this._menu[key].active = false;
            }
        }
        this.updateSubmenu(item, this._complejoDeportivo)
    }

    updateSubmenu(item, submenu){
        submenu.forEach(element => {
            if(element.itemName == item){
                element.active = true;
            } else {
                element.active = false;
            }
        });
    }

    reloadCollapsableMenus() {
        // Se debe actualizar el modelo que se envia al componente
        // this._complejoDeportivo = this.getMenuComplejo();
    }
 }
 
 export default MenuModel;