class AdminSidebarController {

    constructor($state, $timeout, menuModel) {
        'ngInject';
        this.$state = $state;
        this.$timeout = $timeout;
        this.menuModel = menuModel;
        
        $timeout(() => {
            this.menuModel.menu = this.getMenuItems();
            this.menuModel.complejoDeportivo = this.getMenuComplejoDeportivo();

            let currentState = $state.current.name.split('.');
            this.menuModel.setActiveItem(currentState[currentState.length-1]);
        }, 1)
    }

    navegar(item) {
        console.log('Navega a: ' + this.menuModel.menu[item].state);
        this.$state.go(this.menuModel.menu[item].state);
        this.menuModel.setActiveItem(item);
    }

    getMenuItems() {
        return {
            // dashboard
            dashboard: {
                active: true,
                state: 'app.admin.dashboard'
            },
            // alquiler
            alquiler: {
                active: false,
                state: 'app.admin.alquiler'
            },
            // complejo
            franjasHorarias: {
                active: false,
                state: 'app.admin.complejo.franjasHorarias'
            },
            canchas: {
                active: false,
                state: 'app.admin.complejo.canchas'
            },
            precios: {
                active: false,
                state: 'app.admin.complejo.precios'
            },
            estado: {
                active: false,
                state: 'app.admin.complejo.estado'
            },
            // Puntos fidelidad
            puntos: {
                active: false,
                state: 'app.admin.puntos'
            },
            // Notificaciones
            notificaciones: {
                active: false,
                state: 'app.admin.notificaciones'
            },
            // Cuenta 
            cuenta: {
                active: false,
                state: 'app.admin.cuenta'
            },
            // Pagos
            pagos: {
                active: false,
                state: 'app.admin.pagos'
            },
            // Usuarios
            usuarios: {
                active: false,
                state: 'app.admin.usuarios'
            }
        }
    }

    getMenuComplejoDeportivo() {
        return [
            {
                itemName: 'franjasHorarias',
                label: 'Franjas horarias',
                action: () => {
                    this.navegar('franjasHorarias')
                },
                active: this.menuModel.menu.franjasHorarias.active
            }, {
                itemName: 'canchas',
                label: 'Administrar canchas',
                action: () => {
                    this.navegar('canchas')
                },
                active: this.menuModel.menu.canchas.active
            }, {
                itemName: 'precios',
                label: 'Configurar precios',
                action: () => {
                    this.navegar('precios')
                },
                active: this.menuModel.menu.precios.active
            }, {
                itemName: 'estado',
                label: 'Estado (Mantenimiento)',
                action: () => {
                    this.navegar('estado')
                },
                active: this.menuModel.menu.estado.active
            }
        ]
    }

}

export default AdminSidebarController;
