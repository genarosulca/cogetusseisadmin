class AdminHeaderController {

    constructor($state, $timeout, notificacionesModel, menuModel) {
        'ngInject';
        this.$state = $state;
        this.notificacionesModel = notificacionesModel;
        this.menuModel = menuModel;
        this.status = {};
        this.status.isopen = false;

        $timeout(() => {
            this.appendTo =  angular.element(document.getElementById("notifDropdownAppend"));
        }, 1);
    }

    toggleNotification($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    verTodasNotificaciones(){
        console.log('Ver todas');
        this.menuModel.setActiveItem('notificaciones');
        this.$state.go('app.admin.notificaciones')
    }

}

export default AdminHeaderController;
