class AdminCenterController{

    constructor(notificacionesModel, $state){
        'ngInject';
        this.notificacionesModel = notificacionesModel;
        this.notificacionesModel.notificaciones = this.getNotificaciones();
        this.notificacionesModel.nuevas = 2;
        this.$state = $state;
    }

    getNotificaciones(){
        // new Date(Date.UTC(2018, 0, 16));
        this.minutos = new Date();
        this.minutos.setMinutes(this.minutos.getMinutes() - 1)

        this.horas = new Date();
        this.horas.setHours(this.horas.getHours() - 1)

        return [
            {
                type: 'error',
                from: 'Juan Sulca',
                title: 'Para que quiere saber eso. Saludos.',
                date: new Date(),
                action: () => {
                    console.log('Ver notificacion error')
                },
                readed: false
            },
            {
                type: 'success',
                from: 'Fernando',
                title: 'Que te vaya muy bien mijin',
                date: new Date(this.minutos),
                action: () => {
                    console.log('Ver notificacion Fernando')
                },
                readed: false
            }, 
            {
                type: 'success',
                from: 'Tania',
                title: 'Esa es guambra!!!!',
                date: new Date(this.horas),
                action: () => {
                    console.log('Ver notificacion Tania')
                },
                readed: true
            }, 
            {
                type: 'success',
                from: 'Danny',
                title: 'Felicitaciones',
                date: new Date(Date.UTC(2018, 0, 3)),
                action: () => {
                    console.log('Ver notificacion Danny')
                },
                readed: true
            }, 
            {
                type: 'error',
                from: 'Ana',
                title: 'Saludos desde Sangolqui',
                date: new Date(Date.UTC(2017, 11, 30)),
                action: () => {
                    console.log('Ver notificacion Ana')
                },
                readed: true
            }, 
            {
                type: 'error',
                from: 'Sandy',
                title: 'Saludos desde Ruminahui',
                date: new Date(Date.UTC(2016, 11, 16)),
                action: () => {
                    console.log('Ver notificacion Sandy')
                },
                readed: true
            }, 
            {
                type: 'error',
                from: 'Juan Sulca',
                title: 'Para que quiere saber eso. Saludos.',
                date: new Date(),
                action: () => {
                    console.log('Ver notificacion error')
                },
                readed: false
            },
            {
                type: 'success',
                from: 'Fernando',
                title: 'Que te vaya muy bien mijin',
                date: new Date(this.minutos),
                action: () => {
                    console.log('Ver notificacion Fernando')
                },
                readed: false
            }, 
            {
                type: 'success',
                from: 'Tania',
                title: 'Esa es guambra!!!!',
                date: new Date(this.horas),
                action: () => {
                    console.log('Ver notificacion Tania')
                },
                readed: true
            }, 
        ]
    }

    verTodas(){
        this.$state.go('app.admin.notificaciones')
    }
}

export default AdminCenterController;
