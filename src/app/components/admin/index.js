import adminModule from './adminModule';

import AdminHeaderController from './controllers/adminHeaderController';
adminModule.controller('adminHeaderController', AdminHeaderController);
import AdminSidebarController from './controllers/adminSidebarController';
adminModule.controller('adminSidebarController', AdminSidebarController);
import AdminCenterController from './controllers/adminCenterController';
adminModule.controller('adminCenterController', AdminCenterController);

import AdminNotificacionesController from './controllers/adminNotificacionesController';
adminModule.controller('adminNotificacionesController', AdminNotificacionesController);

import NotificacionesModel from './models/notificacionesModel';
adminModule.factory('notificacionesModel',  () => new NotificacionesModel())
import MenuModel from './models/menuModel';
adminModule.factory('menuModel',  () => new MenuModel())

export default adminModule;
