import alquilerModule from './alquilerModule';

import AlquilerController from './controllers/alquilerController';
alquilerModule.controller('alquilerController', AlquilerController);

export default alquilerModule;
