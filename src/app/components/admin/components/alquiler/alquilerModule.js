import angular from 'angular';

import '@uirouter/angularjs';
import './assets/alquiler.css';

import alquilerTpl from './views/alquilerView.html';

let alquilerModule = angular.module('app.admin.alquiler', [
    'ui.router',
]);

alquilerModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.alquiler', {
            url: '/alquiler',
            views: {
                'center@app': {
                    controller: 'alquilerController',
                    controllerAs: 'alquiler',
                    template: alquilerTpl
                }
            }
        })
        ;
});

export default alquilerModule;