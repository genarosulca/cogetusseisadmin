import estadoModule from './estadoModule';

import EstadoController from './controllers/estadoController';
estadoModule.controller('estadoController', EstadoController);

export default estadoModule;
