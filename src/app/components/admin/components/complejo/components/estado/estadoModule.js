import angular from 'angular';

import '@uirouter/angularjs';
import './assets/estado.css';

import estadoTpl from './views/estadoView.html';

let estadoModule = angular.module('app.admin.complejo.estado', [
    'ui.router',
]);

estadoModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.complejo.estado', {
            url: '/estado',
            views: {
                'center@app': {
                    controller: 'estadoController',
                    controllerAs: 'estado',
                    template: estadoTpl
                }
            }
        })
        ;
});

export default estadoModule;