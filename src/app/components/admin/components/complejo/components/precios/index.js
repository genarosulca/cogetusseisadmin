import preciosModule from './preciosModule';

import PreciosController from './controllers/preciosController';
preciosModule.controller('preciosController', PreciosController);

export default preciosModule;
