import angular from 'angular';

import '@uirouter/angularjs';
import './assets/precios.css';

import preciosTpl from './views/preciosView.html';

let preciosModule = angular.module('app.admin.complejo.precios', [
    'ui.router',
]);

preciosModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.complejo.precios', {
            url: '/precios',
            views: {
                'center@app': {
                    controller: 'preciosController',
                    controllerAs: 'precios',
                    template: preciosTpl
                }
            }
        })
        ;
});

export default preciosModule;