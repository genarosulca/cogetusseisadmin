import franjasHorariasModule from './franjasHorariasModule';

import FranjasHorariasController from './controllers/franjasHorariasController';
franjasHorariasModule.controller('franjasHorariasController', FranjasHorariasController);

export default franjasHorariasModule;
