import angular from 'angular';

import '@uirouter/angularjs';
import './assets/franjasHorarias.css';

import franjasHorariasTpl from './views/franjasHorariasView.html';

let franjasHorariasModule = angular.module('app.admin.complejo.franjasHorarias', [
    'ui.router',
]);

franjasHorariasModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.complejo.franjasHorarias', {
            url: '/franjasHorarias',
            views: {
                'center@app': {
                    controller: 'franjasHorariasController',
                    controllerAs: 'franjasHorarias',
                    template: franjasHorariasTpl
                }
            }
        })
        ;
});

export default franjasHorariasModule;