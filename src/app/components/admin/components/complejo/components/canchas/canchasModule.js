import angular from 'angular';

import '@uirouter/angularjs';
import './assets/canchas.css';

import canchasTpl from './views/canchasView.html';

let canchasModule = angular.module('app.admin.complejo.canchas', [
    'ui.router',
]);

canchasModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.complejo.canchas', {
            url: '/canchas',
            views: {
                'center@app': {
                    controller: 'canchasController',
                    controllerAs: 'canchas',
                    template: canchasTpl
                }
            }
        })
        ;
});

export default canchasModule;