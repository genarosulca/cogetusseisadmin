import canchasModule from './canchasModule';

import CanchasController from './controllers/canchasController';
canchasModule.controller('canchasController', CanchasController);

export default canchasModule;
