import angular from 'angular';

import '@uirouter/angularjs';

// importo submodulos
import  './components/franjasHorarias';
import  './components/canchas';
import  './components/precios';
import  './components/estado';

let complejoModule = angular.module('app.admin.complejo', [
    'ui.router',
    // Submodulos
    'app.admin.complejo.franjasHorarias',
    'app.admin.complejo.canchas',
    'app.admin.complejo.precios',
    'app.admin.complejo.estado'
]);

complejoModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.complejo', {
            url: '/complejo',
            abstract: true
            // views: {
            //     'center@app': {
            //         controller: 'complejoController',
            //         controllerAs: 'complejo',
            //         template: complejoTpl
            //     }
            // }
        })
        ;
});

export default complejoModule;