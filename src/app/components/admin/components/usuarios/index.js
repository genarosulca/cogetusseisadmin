import usuariosModule from './usuariosModule';

import UsuariosController from './controllers/usuariosController';
usuariosModule.controller('usuariosController', UsuariosController);

export default usuariosModule;
