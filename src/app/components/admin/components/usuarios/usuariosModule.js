import angular from 'angular';

import '@uirouter/angularjs';

import usuariosTpl from './views/usuariosView.html';
import './assets/usuarios.css'

let usuariosModule = angular.module('app.admin.usuarios', [
    'ui.router',
]);

usuariosModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.usuarios', {
            url: '/usuarios',
            views: {
                'center@app': {
                    controller: 'usuariosController',
                    controllerAs: 'usuarios',
                    template: usuariosTpl
                }
            }
        })
        ;
});

export default usuariosModule;