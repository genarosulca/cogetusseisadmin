import angular from 'angular';

import '@uirouter/angularjs';

import dashboardTpl from './views/dashboardView.html';
import './assets/dashboard.css'

let dashboardModule = angular.module('app.admin.dashboard', [
    'ui.router',
]);

dashboardModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.dashboard', {
            url: '/dashboard',
            views: {
                'center@app': {
                    controller: 'dashboardController',
                    controllerAs: 'dashboard',
                    template: dashboardTpl
                }
            }
        })
        ;
});

export default dashboardModule;