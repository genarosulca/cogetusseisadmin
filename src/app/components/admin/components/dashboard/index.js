import dashboardModule from './dashboardModule';

import DashboardController from './controllers/dashboardController';
dashboardModule.controller('dashboardController', DashboardController);

export default dashboardModule;
