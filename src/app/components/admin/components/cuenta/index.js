import cuentaModule from './cuentaModule';

import CuentaController from './controllers/cuentaController';
cuentaModule.controller('cuentaController', CuentaController);

export default cuentaModule;
