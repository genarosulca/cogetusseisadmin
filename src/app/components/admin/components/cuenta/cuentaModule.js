import angular from 'angular';

import '@uirouter/angularjs';

import cuentaTpl from './views/cuentaView.html';
import './assets/cuenta.css'

let cuentaModule = angular.module('app.admin.cuenta', [
    'ui.router',
]);

cuentaModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.cuenta', {
            url: '/cuenta',
            views: {
                'center@app': {
                    controller: 'cuentaController',
                    controllerAs: 'cuenta',
                    template: cuentaTpl
                }
            }
        })
        ;
});

export default cuentaModule;