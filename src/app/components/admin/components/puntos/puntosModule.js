import angular from 'angular';

import '@uirouter/angularjs';

import puntosTpl from './views/puntosView.html';
import './assets/puntos.css'

let puntosModule = angular.module('app.admin.puntos', [
    'ui.router',
]);

puntosModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.puntos', {
            url: '/puntos',
            views: {
                'center@app': {
                    controller: 'puntosController',
                    controllerAs: 'puntos',
                    template: puntosTpl
                }
            }
        })
        ;
});

export default puntosModule;