import puntosModule from './puntosModule';

import PuntosController from './controllers/puntosController';
puntosModule.controller('puntosController', PuntosController);

export default puntosModule;
