import angular from 'angular';

import '@uirouter/angularjs';

import pagosTpl from './views/pagosView.html';
import './assets/pagos.css'

let pagosModule = angular.module('app.admin.pagos', [
    'ui.router',
]);

pagosModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.pagos', {
            url: '/pagos',
            views: {
                'center@app': {
                    controller: 'pagosController',
                    controllerAs: 'pagos',
                    template: pagosTpl
                }
            }
        })
        ;
});

export default pagosModule;