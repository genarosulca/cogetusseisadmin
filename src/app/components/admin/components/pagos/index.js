import pagosModule from './pagosModule';

import PagosController from './controllers/pagosController';
pagosModule.controller('pagosController', PagosController);

export default pagosModule;
