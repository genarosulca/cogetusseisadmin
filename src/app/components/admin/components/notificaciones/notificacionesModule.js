import angular from 'angular';

import '@uirouter/angularjs';

import notificacionesTpl from './views/notificacionesView.html';
import './assets/notificaciones.css'

let notificacionesModule = angular.module('app.admin.notificaciones', [
    'ui.router',
]);

notificacionesModule.config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
        .state('app.admin.notificaciones', {
            url: '/notificaciones',
            views: {
                'center@app': {
                    controller: 'notificacionesController',
                    controllerAs: 'notificaciones',
                    template: notificacionesTpl
                }
            }
        })
        ;
});

export default notificacionesModule;