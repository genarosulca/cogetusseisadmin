import notificacionesModule from './notificacionesModule';

import NotificacionesController from './controllers/notificacionesController';
notificacionesModule.controller('notificacionesController', NotificacionesController);

export default notificacionesModule;
