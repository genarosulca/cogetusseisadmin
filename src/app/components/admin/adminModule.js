import angular from 'angular';

import '@uirouter/angularjs';

import './assets/admin.css';

import adminHeaderTpl from './views/adminHeaderView.html';
import adminSidebarTpl from './views/adminSidebarView.html';
import adminCenterTpl from './views/adminCenterView.html';
import adminNotificacionesTpl from './views/adminNotificacionesView.html';

// importo submodulos
import  './components/dashboard';
import './components/alquiler';
import './components/complejo';
import './components/puntos';
import './components/notificaciones';
import './components/cuenta';
import './components/pagos';
import './components/usuarios';

let loginModule = angular.module('app.admin', [
    'ui.router',
    // Submodulos
    'app.admin.dashboard',
    'app.admin.alquiler',
    'app.admin.complejo',
    'app.admin.puntos',
    'app.admin.notificaciones',
    'app.admin.cuenta',
    'app.admin.pagos',
    'app.admin.usuarios',
])
;


loginModule.config(($stateProvider, $urlRouterProvider) => {

    $stateProvider
        .state('app.admin', {
            url: 'admin',
            views: {
                'header@app': {
                    controller: 'adminHeaderController',
                    controllerAs: 'adminHeader',
                    template: adminHeaderTpl
                },
                'sidebar@app': {
                    controller: 'adminSidebarController',
                    controllerAs: 'adminSidebar',
                    template: adminSidebarTpl
                },
                'center@app': {
                    controller: 'adminCenterController',
                    controllerAs: 'adminCenter',
                    template: adminCenterTpl
                },
                'notificaciones': {
                    controller: 'adminNotificacionesController',
                    controllerAs: 'adminNotificaciones',
                    template: adminNotificacionesTpl
                }
            }
        })
        ;

});

export default loginModule;