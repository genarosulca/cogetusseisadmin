import loginModule from './loginModule';

import LoginController from './controllers/loginController';
loginModule.controller('loginController', LoginController);

export default loginModule;
