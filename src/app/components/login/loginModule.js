import angular from 'angular';

import '@uirouter/angularjs';

import './assets/login.css';

import loginTpl from './views/loginView.html';

let loginModule = angular.module('app.login', [
    'ui.router',
]);


loginModule.config(($stateProvider, $urlRouterProvider) => {

    $stateProvider
        .state('app.login', {
            url: 'login',
            views: {
                '@': {
                    controller: 'loginController',
                    controllerAs: 'login',
                    template: loginTpl
                },
                // 'sidebar@app': {
                //     controller: 'adminSidebarController',
                //     controllerAs: 'adminSidebar',
                //     template: adminSidebarTpl
                // },
                // 'center@app': {
                //     controller: 'adminCenterController',
                //     controllerAs: 'adminCenter',
                //     template: adminCenterTpl
                // }
            }
            // controller: 'loginController',
            // controllerAs: 'login',
            // template: loginTpl
        })
        ;

});

export default loginModule;