class LoginController {

    constructor($window, $timeout, $state) {
        'ngInject';
        console.log("LoginController")
        this.user = {};
        this.fondo = "/img/fondoLogin.jpg";
        this.errors = [];
        this.$state = $state;
    }

    ingresar(loginForm) {
        if (loginForm.$valid) {
            if (this.user.username === 'Genaro') {
                console.log('Usuario encontrado')
                console.log('**CONTINUANDO AL DASHBOARD**')
                this.$state.go('app.admin')
            } else {
                console.log('Usuario no encontrado')
                loginForm.username.$setValidity("usuarionoencontrado", false);
                this.errors.push(
                    {
                        tipo: 'usuarionoencontrado',
                        msg: 'Usuario no encontrado'
                    }
                )
            }
        }
    }

}

export default LoginController;
